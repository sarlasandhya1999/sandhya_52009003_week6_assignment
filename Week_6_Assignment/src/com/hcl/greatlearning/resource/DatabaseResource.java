package com.hcl.greatlearning.resource;

import java.sql.Connection;
import java.sql.DriverManager;

public class DatabaseResource {
	
	 private static Connection con;
	 private DatabaseResource() {
		 try {
		 Class.forName("com.mysql.cj.jdbc.Driver");
			con=DriverManager.getConnection("jdbc:mysql://localhost:3306/movie","root","Sandhya@8");
		 }catch(Exception e){
			 System.out.println("Database connection exception");
		 }
	 }
	public static Connection getDatabaseConnection() {
		try {
			return con;
		}catch(Exception e) {
			System.out.println("Database Connection"+e);
		}
		return null;
	}

}
