package com.hcl.greatlearning.test;
import static org.junit.Assert.*;

import org.junit.Test;
import com.hcl.greatlearning.bean.Top_Rated_Movies;

public class Top_Rated_Movies_Test {
	//@Test
	public void testTop_Rated_Movie() {
		fail("Not yet implemented");
	}

	//@Test
	public void testTop_Rated_MovieStringIntStringStringIntInt() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetTitle() {
		System.out.println("getTitle");
		Top_Rated_Movies trm=new Top_Rated_Movies();
		String expResult="Det Sjunde Inseglet";
		trm.setTitle("Det Sjunde Inseglet");
		String result=trm.getTitle();
		assertEquals(expResult,result);
	}

	@Test
	public void testSetTitle() {
	
		System.out.println("setTitle");
		String title="Det Sjunde Insiglet";
		Top_Rated_Movies trm=new Top_Rated_Movies();
		trm.setTitle(title);
		assertEquals(trm.getTitle(),title);
	}

	@Test
	public void testGetYear() {
		System.out.println("getYear");
		Top_Rated_Movies trm=new Top_Rated_Movies();
		int expResult=1957;
		trm.setYear(1957);
		int result=trm.getYear();
		assertEquals(expResult,result);
	}

	@Test
	public void testSetYear() {
		
		System.out.println("setYear");
		int year=1957;
		Top_Rated_Movies trm=new Top_Rated_Movies();
		trm.setYear(year);
		assertEquals(trm.getYear(),year);
	}

	@Test
	public void testGetGenres() {
		System.out.println("getGenres");
		Top_Rated_Movies trm=new Top_Rated_Movies();
		String expResult="Fantasy";
		trm.setGenres("Fantasy");
		String result=trm.getGenres();
		assertEquals(expResult,result);
	}

	@Test
	public void testSetGenres() {
	
		System.out.println("setGenres");
		String genres="Fantasy";
		Top_Rated_Movies trm=new Top_Rated_Movies();
		trm.setGenres(genres);
		assertEquals(trm.getGenres(),genres);
	}

	@Test
	public void testGetDuration() {
		
		System.out.println("getDuration");
		Top_Rated_Movies trm=new Top_Rated_Movies();
		String expResult="PT96M";
		trm.setDuration("PT96M");
		String result=trm.getDuration();
		assertEquals(expResult,result);
	}

	@Test
	public void testSetDuration() {
		System.out.println("setDuration");
		String duration="PT96M";
		Top_Rated_Movies trm=new Top_Rated_Movies();
		trm.setDuration(duration);
		assertEquals(trm.getDuration(),duration);
	}

	@Test
	public void testGetRelease_date() {

		System.out.println("getRelease_date");
		Top_Rated_Movies trm=new Top_Rated_Movies();
		int expResult=1957-02-16;
		trm.setRelease_date(1957-02-16);
		int result=trm.getRelease_date();
		assertEquals(expResult,result);
	}

	@Test
	public void testSetRelease_date() {
		
		System.out.println("setRelease_date");
		int release_date=1957-02-16;
		Top_Rated_Movies trm=new Top_Rated_Movies();
		trm.setRelease_date(release_date);
		assertEquals(trm.getRelease_date(),release_date);
	}

	@Test
	public void testGetImdbRating() {
		//fail("Not yet implemented");
		System.out.println("getImdbRating");
		Top_Rated_Movies trm=new Top_Rated_Movies();
		int expResult=8;
		trm.setImdbRating(8);
		int result=trm.getImdbRating();
		assertEquals(expResult,result);
	}

	@Test
	public void testSetImdbRating() {
		//fail("Not yet implemented");
		System.out.println("setImdbRating");
		int imdbrating=8;
		Top_Rated_Movies trm=new Top_Rated_Movies();
		trm.setImdbRating(imdbrating);
		assertEquals(trm.getImdbRating(),imdbrating);
	}

}
