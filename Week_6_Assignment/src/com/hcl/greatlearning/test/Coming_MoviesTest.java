package com.hcl.greatlearning.test;

import static org.junit.Assert.*;

import org.junit.Test;

import com.hcl.greatlearning.bean.Coming_Movies;


public class Coming_MoviesTest {
	//@Test
	public void testMovie_Coming() {
		fail("Not yet implemented");
	}

	//@Test
	public void testMovie_ComingIntStringIntStringStringInt() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetId() {
		
		System.out.println("getId");
		Coming_Movies mc=new Coming_Movies();
		int expResult=1;
		mc.setId(1);
		int result=mc.getId();
		assertEquals(expResult,result);
		
		
		
	}

	@Test
	public void testSetId() {
		
		System.out.println("setId");
		int id=1;
		Coming_Movies mc=new Coming_Movies();
		mc.setId(id);
		assertEquals(mc.getId(),id);
	}

	@Test
	public void testGetTitle() {
		//fail("Not yet implemented");
		System.out.println("getTitle");
		Coming_Movies mc=new Coming_Movies();
		String expResult="Game Night";
		mc.setTitle("Game Night");
		String result=mc.getTitle();
		assertEquals(expResult,result);
	}

	@Test
	public void testSetTitle() {
		//fail("Not yet implemented");
		System.out.println("setTitle");
		String title="Game Night";
		Coming_Movies mc=new Coming_Movies();
		mc.setTitle(title);
		assertEquals(mc.getTitle(),title);
	}

	@Test
	public void testGetYear() {
		//fail("Not yet implemented");
		System.out.println("getYear");
		Coming_Movies mc=new Coming_Movies();
		int expResult=2018;
		mc.setYear(2018);
		int result=mc.getYear();
		assertEquals(expResult,result);

	}

	@Test
	public void testSetYear() {
		//fail("Not yet implemented");
		System.out.println("setYear");
		int year=2018;
		Coming_Movies mc=new Coming_Movies();
		mc.setYear(year);
		assertEquals(mc.getYear(),year);
	}

	@Test
	public void testGetGenres() {
		//fail("Not yet implemented");
		System.out.println("getGenres");
		Coming_Movies mc=new Coming_Movies();
		String expResult="Action";
		mc.setGenres("Action");
		String result=mc.getGenres();
		assertEquals(expResult,result);
	}

	@Test
	public void testSetGenres() {
		//fail("Not yet implemented");
		System.out.println("setGenres");
		String genres="Action";
		Coming_Movies mc=new Coming_Movies();
		mc.setGenres(genres);
		assertEquals(mc.getGenres(),genres);
		}

	@Test
	public void testGetDuration() {
		//fail("Not yet implemented");
		System.out.println("getDuration");
		Coming_Movies mc=new Coming_Movies();
		String expResult="PT100M";
		mc.setDuration("PT100M");
		String result=mc.getDuration();
		assertEquals(expResult,result);
	}

	@Test
	public void testSetDuration() {
		//fail("Not yet implemented");
		System.out.println("setDuration");
		String duration="PT100M";
		Coming_Movies mc=new Coming_Movies();
		mc.setDuration(duration);
		assertEquals(mc.getDuration(),duration);
	}

	@Test
	public void testGetRelease_date() {
		//fail("Not yet implemented");
		System.out.println("getRelease_date");
		Coming_Movies mc=new Coming_Movies();
		int expResult=2018-02-28;
		mc.setRelease_date(2018-02-28);
		int result=mc.getRelease_date();
		assertEquals(expResult,result);

	}

	@Test
	public void testSetRelease_date() {
		//fail("Not yet implemented");
		System.out.println("setRelease_date");
		int release_date=2018-02-28;
		Coming_Movies mc=new Coming_Movies();
		mc.setRelease_date(release_date);
		assertEquals(mc.getRelease_date(),release_date);
	}


}
