package com.hcl.greatlearning.bean;

public class Coming_Movies {
	private int id;
	private  String title;
	private  int year;
	private String genres;
	private String Duration;
	private int  release_date;
	
	public Coming_Movies() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Coming_Movies(int id, String title, int year, String genres,
			String duration, int release_date) {
		super();
		this.id = id;
		this.title = title;
		this.year = year;
		this.genres = genres;
		Duration = duration;
		this.release_date = release_date;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public String getGenres() {
		return genres;
	}
	public void setGenres(String genres) {
		this.genres = genres;
	}
	public String getDuration() {
		return Duration;
	}
	public void setDuration(String duration) {
		Duration = duration;
	}
	public int getRelease_date() {
		return release_date;
	}
	public void setRelease_date(int release_date) {
		this.release_date = release_date;
	}
	@Override
	public String toString() {
		return "Coming_Movies [id=" + id + ", title=" + title + ", year="
				+ year + ", genres=" + genres + ", Duration=" + Duration
				+ ", release_date=" + release_date + "]";
	}


}
