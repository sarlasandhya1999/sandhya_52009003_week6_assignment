package com.hcl.greatlearning.bean;

public class Top_Rated_Movies {
	private String title;
	private int year;
	private String genres;
	private String duration;
	private int release_date;
	private int imdbRating;
	
	public Top_Rated_Movies() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Top_Rated_Movies(String title, int year, String genres,
			String duration, int release_date, int imdbRating) {
		super();
		this.title = title;
		this.year = year;
		this.genres = genres;
		this.duration = duration;
		this.release_date = release_date;
		this.imdbRating = imdbRating;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public String getGenres() {
		return genres;
	}
	public void setGenres(String genres) {
		this.genres = genres;
	}
	public String getDuration() {
		return duration;
	}
	public void setDuration(String duration) {
		this.duration = duration;
	}
	public int getRelease_date() {
		return release_date;
	}
	public void setRelease_date(int release_date) {
		this.release_date = release_date;
	}
	public int getImdbRating() {
		return imdbRating;
	}
	public void setImdbRating(int imdbRating) {
		this.imdbRating = imdbRating;
	}
	


}
