package com.hcl.greatlearning.factorydesign;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.hcl.greatlearning.bean.Coming_Movies;
import com.hcl.greatlearning.bean.Movies_In_Theaters;
import com.hcl.greatlearning.bean.Top_Rated_Movieindia;
import com.hcl.greatlearning.bean.Top_Rated_Movies;
import com.hcl.greatlearning.resource.DatabaseResource;


interface MovieOnline  {
	
	public List<String> movieType() throws Exception ;

}
//Factory Pattern
class ComingMovie implements MovieOnline{
	//Database Connection
	Connection con=DatabaseResource.getDatabaseConnection();
	public ComingMovie() {}
	@Override
	public List<String> movieType() throws Exception {
		
		try {
			List<Coming_Movies> movies=new ArrayList<Coming_Movies>();
			PreparedStatement pstmt=con.prepareStatement("select * from Movie_Comming");
			ResultSet rs=pstmt.executeQuery();
			while(rs.next()) {
				Coming_Movies movie=new Coming_Movies();
				movie.setId(rs.getInt(1));
				movie.setTitle(rs.getString(2));
				movie.setYear(rs.getInt(3));
				movie.setGenres(rs.getString(4));
				movie.setDuration(rs.getString(5));
				movie.setRelease_date(rs.getInt(6));
				movies.add(movie);
			}}catch(Exception e) {
				System.out.println("Coming_Movies"+e);
			}
			
		
		return null;
	}
	
}
class MovieInTheaters implements MovieOnline{
	//Database Connection
	Connection con=DatabaseResource.getDatabaseConnection();
	public MovieInTheaters() {}
	@Override
	public List<String> movieType() throws Exception {
		try {
			List<Movies_In_Theaters> movies=new ArrayList<Movies_In_Theaters>();
			PreparedStatement pstmt=con.prepareStatement("select * from Movies_In_Theaters");
			ResultSet rs=pstmt.executeQuery();
			while(rs.next()) {
				Movies_In_Theaters movie=new Movies_In_Theaters();
				movie.setId(rs.getInt(1));
				movie.setTitle(rs.getString(2));
				movie.setYear(rs.getInt(3));
				movie.setGenres(rs.getString(4));
				movie.setDuration(rs.getString(5));
				movie.setRelease_date(rs.getInt(6));
				movies.add(movie);
				
			}
		}catch(Exception e) {
			System.out.println("Movies_In_Theaters"+e);
		}
	    return null;	
	}
	
}
class TopRatedMovieindia implements MovieOnline{
	Connection con=DatabaseResource.getDatabaseConnection();
	public TopRatedMovieindia() {}
	@Override
	public List<String> movieType() throws Exception {
		try {
			List<Top_Rated_Movieindia> movies=new ArrayList<Top_Rated_Movieindia>();
			PreparedStatement pstmt=con.prepareStatement("select * from Top_Rated_Indianmovie");
			ResultSet rs=pstmt.executeQuery();
			while(rs.next()) {
				Top_Rated_Movieindia movie=new Top_Rated_Movieindia();
				movie.setTitle(rs.getString(1));
				movie.setYear(rs.getInt(2));
				movie.setGenres(rs.getString(3));
				movie.setDuration(rs.getString(4));
				movie.setRelease_date(rs.getInt(5));
				movie.setImdbRating(rs.getInt(6));
				
				movies.add(movie);
				
			}
		}catch(Exception e) {
			System.out.println("Top_Rated_Movieindia"+e);
		}
	    return null;	
	}
	
	
}
class TopRatedMovies implements MovieOnline{
	Connection con=DatabaseResource.getDatabaseConnection();
	public TopRatedMovies() {}
	@Override
	public List<String> movieType() throws Exception {
		try {
			List<Top_Rated_Movies> movies=new ArrayList<Top_Rated_Movies>();
			PreparedStatement pstmt=con.prepareStatement("select * from Top_Rated_Movies");
			ResultSet rs=pstmt.executeQuery();
			while(rs.next()) {
				Top_Rated_Movies movie=new Top_Rated_Movies();
				movie.setTitle(rs.getString(1));
				movie.setYear(rs.getInt(2));
				movie.setGenres(rs.getString(3));
				movie.setDuration(rs.getString(4));
				movie.setRelease_date(rs.getInt(5));
				movie.setImdbRating(rs.getInt(6));
				movies.add(movie);
				
			}
		}catch(Exception e) {
			System.out.println("Top_Rated_Movies"+e);
		}
	    return null;	
	}
}
	
class MovieFactory{
	public static MovieOnline getInstance(String type) {
		if(type.equalsIgnoreCase("ComingMovie")) {
			return new ComingMovie();
		}else if(type.equalsIgnoreCase("MovieInTheatres")) {
			return new MovieInTheaters();
		}else if(type.equalsIgnoreCase("TopRatedMovieindia")) {
			return new TopRatedMovieindia();
		}
		else if(type.equalsIgnoreCase("TopRatedMovies")) {
			return new TopRatedMovies();
		}else {
	    	return null;
	}
}
}


public class FactoryMovies {
	public static void main(String[] args)throws Exception {
		// TODO Auto-generated method stub
		
		MovieOnline mo =MovieFactory.getInstance("MovieComingSoon");
		mo.movieType();
	    MovieOnline mo1 =MovieFactory.getInstance("MovieInTheatres");
	    mo1.movieType();
	    MovieOnline mo2 =MovieFactory.getInstance("TopRatedIdianMovie");
	    mo2.movieType();
	    MovieOnline mo3=MovieFactory.getInstance("TopRatedMovie");
	    mo3.movieType();
			


	}

}
