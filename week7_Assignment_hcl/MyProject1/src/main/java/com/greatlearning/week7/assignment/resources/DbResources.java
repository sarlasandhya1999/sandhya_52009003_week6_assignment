package com.greatlearning.week7.assignment.resources;

import java.sql.Connection;
import java.sql.DriverManager;

public class DbResources {
	static Connection con;
	public static Connection getDbConnection() {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/week7", "root", "sandhya@8");
			return  con;
		} catch (Exception e) {
			System.out.println("db Connection method "+e);
		}
		return null;
	}
}