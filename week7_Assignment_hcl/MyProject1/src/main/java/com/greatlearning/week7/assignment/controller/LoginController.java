package com.greatlearning.week7.assignment.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;



import com.greatlearning.week7.assignment.bean.Login;
import com.greatlearning.week7.assignment.dao.LoginDetailsDao;

/**
 * Servlet implementation class LoginController
 */
@WebServlet("/LoginController")
public class LoginController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public static Login currentUser;
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		String name=request.getParameter("name");
		String password=request.getParameter("password");
		
		Login ld=new Login();
		ld.setName(name);
		ld.setPassword(password);
		try {
			if(LoginDetailsDao.validate(ld)) {
				 RequestDispatcher rd = request.getRequestDispatcher("logindetails.jsp"); 
				HttpSession session=request.getSession();
				session.setAttribute("username", name);
				response.sendRedirect("userdetails.jsp");
			}else {
				HttpSession session=request.getSession();
				RequestDispatcher rd=request.getRequestDispatcher("invalid.jsp");
				rd.include(request, response);
				
			}
		} catch (ClassNotFoundException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
	}



    